import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

plugins {
    id("java")
    id("org.spongepowered.plugin") version "0.9.0"
    id("com.github.johnrengelman.shadow") version "4.0.4"
    id("maven-publish")
}

publishing {
    repositories {
        maven {
            name = "GitHubPackages"
            url = uri("https://maven.pkg.github.com/pietelite/griefalert")
            credentials {
                username = project.findProperty("gpr.user").toString()
                password = project.findProperty("gpr.key").toString()
            }
        }
    }
    publications {
        create<MavenPublication>("gpr") {
            from(components["java"])
        }
    }
}

repositories {
    mavenCentral()
    google()
    maven { url = uri("https://maven.enginehub.org/repo/") }
    maven { url = uri("https://jitpack.io") }

    // prism
    maven { url = uri("https://gitlab.com/api/v4/projects/19312285/packages/maven") }

    flatDir {
        dirs("libs")
    }
}

base.archivesBaseName = "griefalert" // Project Base Name Here
group = "com.minecraftonline.sponge" // This is a MinecraftOnline project
version = "1.4.0" // Project Version

java.sourceCompatibility = JavaVersion.VERSION_1_8
java.targetCompatibility = java.sourceCompatibility

ext["sponge_version"] = "7.2.0" // SpongeAPI version
ext["authors"] = listOf("PietElite", "darkdiplomat", "BastetFurry", "14mRh4X0r")

dependencies {
    // minecraft
    implementation("org.spongepowered:spongeapi:${project.ext["sponge_version"]}")
    implementation("com.sk89q.worldedit:worldedit-core:6.1.4-SNAPSHOT")
    implementation("com.sk89q.worldedit:worldedit-sponge:6.1.7-SNAPSHOT")
    implementation("com.github.randombyte-developer:holograms:v3.2.0")
    implementation("com.helion3:prism:3.0.4")

    // database
    implementation("org.xerial:sqlite-jdbc:3.36.0.3")
    implementation("mysql:mysql-connector-java:8.0.29")
    implementation("org.mariadb.jdbc:mariadb-java-client:3.0.4")

    // testing
    testImplementation("junit:junit:4.13.2")
}

sponge.plugin.meta {
    authors = ext["authors"] as List<String>
}

tasks {
    named<ShadowJar>("shadowJar") {
        configurations = listOf(project.configurations.shadow.orNull)
        baseName = base.archivesBaseName
        classifier = ""
    }

    create<Jar>("apiJar") {
        baseName = "${base.archivesBaseName}-api"
        version = "0.0"
        classifier = "git describe --always".trim()
        from(sourceSets.main) {
            include("com/minecraftonline/griefalert/api/**")
        }
    }

    jar {
        // classifier = "git describe --always".trim()
        manifest.attributes["Implementation-Title"] = name
        manifest.attributes["Implementation-Version"] = version
        manifest.attributes["Implementation-Vendor"] = "MinecraftOnline"
    }
}

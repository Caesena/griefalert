# MCO Grief Alert
# 
# Add the blocks to be watched here (without the #).
# 
# Format is -> USE|DESTROY|INTERACT;namespace:blockName;alertColor(;stealth[;deny][;onlyInList])
# stealth, deny, and onlyInList flags are optional
# onlyInList is a comma separated list of any of 'minecraft:overworld', 'minecraft:nether', or 'minecraft.the_end')
#
# Here are some examples
# USE;lava_bucket;c
# DESTROY;minecraft:diamond_block;3
# INTERACT;minecraft:chest;3
# DESTROY;minecraft:netherrack;c;;;minecraft-overworld,minecraft-end
# -----------------
# Tested in 1.12.2
# -----------------
DESTROY;minecraft:polished_granite;c 			# Not functional yet in 1.12.2
DESTROY;minecraft:polished_andesite;c			# Not functional yet in 1.12.2
DESTROY;minecraft:polished_diorite;c			# Not functional yet in 1.12.2
DESTROY;minecraft:cobblestone;c
DESTROY;minecraft:oak_planks;c				# Not functional yet in 1.12.2
DESTROY;minecraft:spruce_planks;c			# Not functional yet in 1.12.2
DESTROY;minecraft:birch_planks;c			# Not functional yet in 1.12.2
DESTROY;minecraft:jungle_planks;c			# Not functional yet in 1.12.2
DESTROY;minecraft:acacia_planks;c			# Not functional yet in 1.12.2
DESTROY;minecraft:dark_oak_planks;c			# Not functional yet in 1.12.2
DESTROY;minecraft:bedrock;5;;deny
DESTROY;minecraft:gold_ore;c
DESTROY;minecraft:stripped_oak_log;c			# Not functional yet in 1.12.2
DESTROY;minecraft:stripped_spruce_log;c			# Not functional yet in 1.12.2
DESTROY;minecraft:stripped_birch_log;c			# Not functional yet in 1.12.2
DESTROY;minecraft:stripped_jungle_log;c			# Not functional yet in 1.12.2
DESTROY;minecraft:stripped_acacia_log;c			# Not functional yet in 1.12.2
DESTROY;minecraft:stripped_dark_oak_log;c		# Not functional yet in 1.12.2
DESTROY;minecraft:stripped_oak_wood;c			# Not functional yet in 1.12.2
DESTROY;minecraft:stripped_spruce_wood;c		# Not functional yet in 1.12.2
DESTROY;minecraft:stripped_birch_wood;c			# Not functional yet in 1.12.2
DESTROY;minecraft:stripped_jungle_wood;c		# Not functional yet in 1.12.2
DESTROY;minecraft:stripped_acacia_wood;c		# Not functional yet in 1.12.2
DESTROY;minecraft:stripped_dark_oak_wood;c		# Not functional yet in 1.12.2
DESTROY;minecraft:sponge;c
DESTROY;minecraft:wet_sponge;c				# Not functional yet in 1.12.2
DESTROY;minecraft:glass;c
DESTROY;minecraft:lapis_ore;c
DESTROY;minecraft:lapis_block;c
DESTROY;minecraft:dispenser;c
DESTROY;minecraft:sandstone;c
DESTROY;minecraft:chiseled_sandstone;c			# Not functional yet in 1.12.2
DESTROY;minecraft:cut_sandstone;c			# Not functional yet in 1.12.2
DESTROY;minecraft:note_block;c				# Not functional yet in 1.12.2
DESTROY;minecraft:powered_rail;c			# Not functional yet in 1.12.2
DESTROY;minecraft:detector_rail;c
DESTROY;minecraft:sticky_piston;c
DESTROY;minecraft:cobweb;c				# Not functional yet in 1.12.2
DESTROY;minecraft:piston;c
DESTROY;minecraft:white_wool;c				# Not functional yet in 1.12.2
DESTROY;minecraft:orange_wool;c				# Not functional yet in 1.12.2
DESTROY;minecraft:magenta_wool;c			# Not functional yet in 1.12.2
DESTROY;minecraft:light_blue_wool;c			# Not functional yet in 1.12.2
DESTROY;minecraft:yellow_wool;c				# Not functional yet in 1.12.2
DESTROY;minecraft:lime_wool;c				# Not functional yet in 1.12.2
DESTROY;minecraft:pink_wool;c				# Not functional yet in 1.12.2
DESTROY;minecraft:gray_wool;c				# Not functional yet in 1.12.2
DESTROY;minecraft:light_gray_wool;c			# Not functional yet in 1.12.2
DESTROY;minecraft:cyan_wool;c				# Not functional yet in 1.12.2
DESTROY;minecraft:purple_wool;c				# Not functional yet in 1.12.2
DESTROY;minecraft:blue_wool;c				# Not functional yet in 1.12.2
DESTROY;minecraft:brown_wool;c				# Not functional yet in 1.12.2
DESTROY;minecraft:green_wool;c				# Not functional yet in 1.12.2
DESTROY;minecraft:black_wool;c				# Not functional yet in 1.12.2
DESTROY;minecraft:gold_block;c
DESTROY;minecraft:iron_block;c
DESTROY;minecraft:oak_slab;c				# Not functional yet in 1.12.2
DESTROY;minecraft:spruce_slab;c				# Not functional yet in 1.12.2
DESTROY;minecraft:birch_slab;c				# Not functional yet in 1.12.2
DESTROY;minecraft:jungle_slab;c				# Not functional yet in 1.12.2
DESTROY;minecraft:acacia_slab;c				# Not functional yet in 1.12.2
DESTROY;minecraft:dark_oak_slab;c			# Not functional yet in 1.12.2
DESTROY;minecraft:stone_slab;c
DESTROY;minecraft:sandstone_slab;c			# Not functional yet in 1.12.2
DESTROY;minecraft:petrified_oak_slab;c			# Not functional yet in 1.12.2
DESTROY;minecraft:cobblestone_slab;c			# Not functional yet in 1.12.2
DESTROY;minecraft:brick_slab;c				# Not functional yet in 1.12.2
DESTROY;minecraft:stone_brick_slab;c			# Not functional yet in 1.12.2
DESTROY;minecraft:nether_brick_slab;c			# Not functional yet in 1.12.2
DESTROY;minecraft:quartz_slab;c				# Not functional yet in 1.12.2
DESTROY;minecraft:red_sandstone_slab;c			# Not functional yet in 1.12.2
DESTROY;minecraft:purpur_slab;c
DESTROY;minecraft:prismarine_slab;c			# Not functional yet in 1.12.2
DESTROY;minecraft:prismarine_brick_slab;c		# Not functional yet in 1.12.2
DESTROY;minecraft:dark_prismarine_slab;c		# Not functional yet in 1.12.2
DESTROY;minecraft:smooth_quartz;c			# Not functional yet in 1.12.2
DESTROY;minecraft:smooth_red_sandstone;c		# Not functional yet in 1.12.2
DESTROY;minecraft:smooth_sandstone;c
DESTROY;minecraft:smooth_stone;c
DESTROY;minecraft:bricks;c				# Not functional yet in 1.12.2
DESTROY;minecraft:tnt;c
DESTROY;minecraft:bookshelf;c
DESTROY;minecraft:mossy_cobblestone;c
DESTROY;minecraft:obsidian;c
DESTROY;minecraft:torch;c
DESTROY;minecraft:end_rod;c
DESTROY;minecraft:chorus_plant;c
DESTROY;minecraft:chorus_flower;c
DESTROY;minecraft:purpur_block;c			# Not functional yet in 1.12.2
DESTROY;minecraft:purpur_pillar;c
DESTROY;minecraft:purpur_stairs;c
DESTROY;minecraft:spawner;c				# Not functional yet in 1.12.2
DESTROY;minecraft:oak_stairs;c
DESTROY;minecraft:chest;c
DESTROY;minecraft:diamond_ore;c
DESTROY;minecraft:diamond_block;c
DESTROY;minecraft:crafting_table;c
DESTROY;minecraft:farmland;c
DESTROY;minecraft:furnace;c
DESTROY;minecraft:ladder;c
DESTROY;minecraft:rail;c
DESTROY;minecraft:cobblestone_stairs;c			# Not functional yet in 1.12.2
DESTROY;minecraft:lever;c
DESTROY;minecraft:stone_pressure_plate;c
DESTROY;minecraft:oak_pressure_plate;c			# Not functional yet in 1.12.2
DESTROY;minecraft:spruce_pressure_plate;c		# Not functional yet in 1.12.2
DESTROY;minecraft:birch_pressure_plate;c		# Not functional yet in 1.12.2
DESTROY;minecraft:jungle_pressure_plate;c		# Not functional yet in 1.12.2
DESTROY;minecraft:acacia_pressure_plate;c		# Not functional yet in 1.12.2
DESTROY;minecraft:dark_oak_pressure_plate;c		# Not functional yet in 1.12.2
DESTROY;minecraft:redstone_torch;c
DESTROY;minecraft:stone_button;c
DESTROY;minecraft:snow_block;c				# Not functional yet in 1.12.2
DESTROY;minecraft:jukebox;c
DESTROY;minecraft:oak_fence;c				# Not functional yet in 1.12.2
DESTROY;minecraft:spruce_fence;c
DESTROY;minecraft:birch_fence;c
DESTROY;minecraft:jungle_fence;c
DESTROY;minecraft:acacia_fence;c
DESTROY;minecraft:dark_oak_fence;c
DESTROY;minecraft:carved_pumpkin;c			# Not functional yet in 1.12.2
DESTROY;minecraft:netherrack;c;;;minecraft:overworld,minecraft:the_end
DESTROY;minecraft:soul_sand;c
DESTROY;minecraft:glowstone;c
DESTROY;minecraft:jack_o_lantern;c			# Not functional yet in 1.12.2
DESTROY;minecraft:oak_trapdoor;c			# Not functional yet in 1.12.2
DESTROY;minecraft:spruce_trapdoor;c			# Not functional yet in 1.12.2
DESTROY;minecraft:birch_trapdoor;c			# Not functional yet in 1.12.2
DESTROY;minecraft:jungle_trapdoor;c			# Not functional yet in 1.12.2
DESTROY;minecraft:acacia_trapdoor;c			# Not functional yet in 1.12.2
DESTROY;minecraft:dark_oak_trapdoor;c			# Not functional yet in 1.12.2
DESTROY;minecraft:dark_oak_trapdoor;c			# Not functional yet in 1.12.2
DESTROY;minecraft:stone_bricks;c			# Not functional yet in 1.12.2
DESTROY;minecraft:mossy_stone_bricks;c			# Not functional yet in 1.12.2
DESTROY;minecraft:cracked_stone_bricks;c		# Not functional yet in 1.12.2
DESTROY;minecraft:chiseled_stone_bricks;c		# Not functional yet in 1.12.2
DESTROY;minecraft:brown_mushroom_block;c
DESTROY;minecraft:red_mushroom_block;c
DESTROY;minecraft:mushroom_stem;c			# Not functional yet in 1.12.2
DESTROY;minecraft:iron_bars;c
DESTROY;minecraft:glass_pane;c
DESTROY;minecraft:oak_fence_gate;c			# Not functional yet in 1.12.2
DESTROY;minecraft:spruce_fence_gate;c
DESTROY;minecraft:birch_fence_gate;c
DESTROY;minecraft:jungle_fence_gate;c
DESTROY;minecraft:acacia_fence_gate;c
DESTROY;minecraft:dark_oak_fence_gate;c
DESTROY;minecraft:brick_stairs;c
DESTROY;minecraft:stone_brick_stairs;c
DESTROY;minecraft:mycelium;c
DESTROY;minecraft:lily_pad;c				# Not functional yet in 1.12.2
DESTROY;minecraft:nether_bricks;c			# Not functional yet in 1.12.2
DESTROY;minecraft:nether_brick_fence;c
DESTROY;minecraft:nether_brick_stairs;c
DESTROY;minecraft:enchanting_table;c
DESTROY;minecraft:end_portal_frame;c
DESTROY;minecraft:end_stone;c
DESTROY;minecraft:end_stone_bricks;c			# Not functional yet in 1.12.2
DESTROY;minecraft:dragon_egg;c
DESTROY;minecraft:redstone_lamp;c
DESTROY;minecraft:sandstone_stairs;c
DESTROY;minecraft:emerald_ore;c
DESTROY;minecraft:ender_chest;c
DESTROY;minecraft:tripwire_hook;c
DESTROY;minecraft:emerald_block;c
DESTROY;minecraft:spruce_stairs;c
DESTROY;minecraft:birch_stairs;c
DESTROY;minecraft:jungle_stairs;c
DESTROY;minecraft:command_block;5;;deny
INTERACT;minecraft:command_block;c
DESTROY;minecraft:beacon;c
DESTROY;minecraft:cobblestone_wall;c
DESTROY;minecraft:mossy_cobblestone_wall;c		# Not functional yet in 1.12.2
DESTROY;minecraft:oak_button;c				# Not functional yet in 1.12.2
DESTROY;minecraft:spruce_button;c			# Not functional yet in 1.12.2
DESTROY;minecraft:birch_button;c			# Not functional yet in 1.12.2
DESTROY;minecraft:jungle_button;c			# Not functional yet in 1.12.2
DESTROY;minecraft:acacia_button;c			# Not functional yet in 1.12.2
DESTROY;minecraft:dark_oak_button;c			# Not functional yet in 1.12.2
DESTROY;minecraft:anvil;c
DESTROY;minecraft:chipped_anvil;c			# Not functional yet in 1.12.2
DESTROY;minecraft:damaged_anvil;c			# Not functional yet in 1.12.2
DESTROY;minecraft:trapped_chest;c
DESTROY;minecraft:light_weighted_pressure_plate;c
DESTROY;minecraft:heavy_weighted_pressure_plate;c
DESTROY;minecraft:daylight_detector;c
DESTROY;minecraft:redstone_block;c
DESTROY;minecraft:nether_quartz_ore;c;;;minecraft:overworld,minecraft:the_end	# Not functional yet in 1.12.2
DESTROY;minecraft:hopper;c
DESTROY;minecraft:chiseled_quartz_block;c		# Not functional yet in 1.12.2
DESTROY;minecraft:quartz_block;c
DESTROY;minecraft:quartz_pillar;c
DESTROY;minecraft:quartz_stairs;c
DESTROY;minecraft:activator_rail;c
DESTROY;minecraft:dropper;c
DESTROY;minecraft:white_terracotta;c			# Not functional yet in 1.12.2
DESTROY;minecraft:orange_terracotta;c			# Not functional yet in 1.12.2
DESTROY;minecraft:magenta_terracotta;c			# Not functional yet in 1.12.2
DESTROY;minecraft:light_blue_terracotta;c		# Not functional yet in 1.12.2
DESTROY;minecraft:yellow_terracotta;c			# Not functional yet in 1.12.2
DESTROY;minecraft:lime_terracotta;c			# Not functional yet in 1.12.2
DESTROY;minecraft:pink_terracotta;c			# Not functional yet in 1.12.2
DESTROY;minecraft:gray_terracotta;c			# Not functional yet in 1.12.2
DESTROY;minecraft:purple_terracotta;c			# Not functional yet in 1.12.2
DESTROY;minecraft:blue_terracotta;c			# Not functional yet in 1.12.2
DESTROY;minecraft:brown_terracotta;c			# Not functional yet in 1.12.2
DESTROY;minecraft:green_terracotta;c			# Not functional yet in 1.12.2
DESTROY;minecraft:red_terracotta;c			# Not functional yet in 1.12.2
DESTROY;minecraft:black_terracotta;c			# Not functional yet in 1.12.2
DESTROY;minecraft:barrier;5;;deny
DESTROY;minecraft:iron_trapdoor;c
DESTROY;minecraft:hay_block;c
DESTROY;minecraft:white_carpet;c			# Not functional yet in 1.12.2
DESTROY;minecraft:orange_carpet;c			# Not functional yet in 1.12.2
DESTROY;minecraft:magenta_carpet;c			# Not functional yet in 1.12.2
DESTROY;minecraft:light_blue_carpet;c			# Not functional yet in 1.12.2
DESTROY;minecraft:yellow_carpet;c			# Not functional yet in 1.12.2
DESTROY;minecraft:lime_carpet;c				# Not functional yet in 1.12.2
DESTROY;minecraft:pink_carpet;c				# Not functional yet in 1.12.2
DESTROY;minecraft:gray_carpet;c				# Not functional yet in 1.12.2
DESTROY;minecraft:light_gray_carpet;c			# Not functional yet in 1.12.2
DESTROY;minecraft:cyan_carpet;c				# Not functional yet in 1.12.2
DESTROY;minecraft:purple_carpet;c			# Not functional yet in 1.12.2
DESTROY;minecraft:blue_carpet;c				# Not functional yet in 1.12.2
DESTROY;minecraft:brown_carpet;c			# Not functional yet in 1.12.2
DESTROY;minecraft:green_carpet;c			# Not functional yet in 1.12.2
DESTROY;minecraft:red_carpet;c				# Not functional yet in 1.12.2
DESTROY;minecraft:black_carpet;c			# Not functional yet in 1.12.2
DESTROY;minecraft:terracotta;c				# Not functional yet in 1.12.2
DESTROY;minecraft:coal_block;c
DESTROY;minecraft:packed_ice;c
DESTROY;minecraft:acacia_stairs;c
DESTROY;minecraft:dark_oak_stairs;c
DESTROY;minecraft:slime_block;c				# Not functional yet in 1.12.2
DESTROY;minecraft:grass_path;c
DESTROY;minecraft:white_stained_glass;c			# Not functional yet in 1.12.2
DESTROY;minecraft:orange_stained_glass;c		# Not functional yet in 1.12.2
DESTROY;minecraft:magenta_stained_glass;c		# Not functional yet in 1.12.2
DESTROY;minecraft:light_blue_stained_glass;c		# Not functional yet in 1.12.2
DESTROY;minecraft:yellow_stained_glass;c		# Not functional yet in 1.12.2
DESTROY;minecraft:lime_stained_glass;c			# Not functional yet in 1.12.2
DESTROY;minecraft:pink_stained_glass;c			# Not functional yet in 1.12.2
DESTROY;minecraft:gray_stained_glass;c			# Not functional yet in 1.12.2
DESTROY;minecraft:light_gray_stained_glass;c		# Not functional yet in 1.12.2
DESTROY;minecraft:cyan_stained_glass;c			# Not functional yet in 1.12.2
DESTROY;minecraft:purple_stained_glass;c		# Not functional yet in 1.12.2
DESTROY;minecraft:blue_stained_glass;c			# Not functional yet in 1.12.2
DESTROY;minecraft:brown_stained_glass;c			# Not functional yet in 1.12.2
DESTROY;minecraft:green_stained_glass;c			# Not functional yet in 1.12.2
DESTROY;minecraft:red_stained_glass;c			# Not functional yet in 1.12.2
DESTROY;minecraft:black_stained_glass;c			# Not functional yet in 1.12.2
DESTROY;minecraft:white_stained_glass_pane;c		# Not functional yet in 1.12.2
DESTROY;minecraft:orange_stained_glass_pane;c		# Not functional yet in 1.12.2
DESTROY;minecraft:magenta_stained_glass_pane;c		# Not functional yet in 1.12.2
DESTROY;minecraft:light_blue_stained_glass_pane;c	# Not functional yet in 1.12.2
DESTROY;minecraft:yellow_stained_glass_pane;c		# Not functional yet in 1.12.2
DESTROY;minecraft:lime_stained_glass_pane;c		# Not functional yet in 1.12.2
DESTROY;minecraft:pink_stained_glass_pane;c		# Not functional yet in 1.12.2
DESTROY;minecraft:gray_stained_glass_pane;c		# Not functional yet in 1.12.2
DESTROY;minecraft:light_gray_stained_glass_pane;c	# Not functional yet in 1.12.2
DESTROY;minecraft:cyan_stained_glass_pane;c		# Not functional yet in 1.12.2
DESTROY;minecraft:purple_stained_glass_pane;c		# Not functional yet in 1.12.2
DESTROY;minecraft:blue_stained_glass_pane;c		# Not functional yet in 1.12.2
DESTROY;minecraft:brown_stained_glass_pane;c		# Not functional yet in 1.12.2
DESTROY;minecraft:green_stained_glass_pane;c		# Not functional yet in 1.12.2
DESTROY;minecraft:red_stained_glass_pane;c		# Not functional yet in 1.12.2
DESTROY;minecraft:black_stained_glass_pane;c		# Not functional yet in 1.12.2
DESTROY;minecraft:prismarine;c
DESTROY;minecraft:prismarine_bricks;c			# Not functional yet in 1.12.2
DESTROY;minecraft:dark_prismarine;c			# Not functional yet in 1.12.2
DESTROY;minecraft:prismarine_stairs;c			# Not functional yet in 1.12.2
DESTROY;minecraft:prismarine_brick_stairs;c		# Not functional yet in 1.12.2
DESTROY;minecraft:dark_prismarine_stairs;c		# Not functional yet in 1.12.2
DESTROY;minecraft:sea_lantern;c				# Not functional yet in 1.12.2
DESTROY;minecraft:red_sandstone;c
DESTROY;minecraft:chiseled_red_sandstone;c		# Not functional yet in 1.12.2
DESTROY;minecraft:cut_red_sandstone;c			# Not functional yet in 1.12.2
DESTROY;minecraft:red_sandstone_stairs;c
DESTROY;minecraft:repeating_command_block;5;;deny
INTERACT;minecraft:repeating_command_block;c
DESTROY;minecraft:chain_command_block;5;;deny
INTERACT;minecraft:chain_command_block;c
DESTROY;minecraft:magma_block;c				# Not functional yet in 1.12.2
DESTROY;minecraft:nether_wart_block;c
DESTROY;minecraft:red_nether_bricks;c			# Not functional yet in 1.12.2
DESTROY;minecraft:bone_block;c
DESTROY;minecraft:structure_void;5;;deny
DESTROY;minecraft:observer;c
DESTROY;minecraft:shulker_box;c				# Not functional yet in 1.12.2
DESTROY;minecraft:white_shulker_box;c
DESTROY;minecraft:orange_shulker_box;c
DESTROY;minecraft:magenta_shulker_box;c
DESTROY;minecraft:light_blue_shulker_box;c
DESTROY;minecraft:yellow_shulker_box;c
DESTROY;minecraft:lime_shulker_box;c
DESTROY;minecraft:pink_shulker_box;c
DESTROY;minecraft:gray_shulker_box;c
DESTROY;minecraft:light_gray_shulker_box;c
DESTROY;minecraft:cyan_shulker_box;c
DESTROY;minecraft:purple_shulker_box;c
DESTROY;minecraft:blue_shulker_box;c
DESTROY;minecraft:brown_shulker_box;c
DESTROY;minecraft:green_shulker_box;c
DESTROY;minecraft:red_shulker_box;c
DESTROY;minecraft:black_shulker_box;c
DESTROY;minecraft:white_glazed_terracotta;c
DESTROY;minecraft:orange_glazed_terracotta;c
DESTROY;minecraft:magenta_glazed_terracotta;c
DESTROY;minecraft:light_blue_glazed_terracotta;c
DESTROY;minecraft:yellow_glazed_terracotta;c
DESTROY;minecraft:lime_glazed_terracotta;c
DESTROY;minecraft:pink_glazed_terracotta;c
DESTROY;minecraft:gray_glazed_terracotta;c
DESTROY;minecraft:light_gray_glazed_terracotta;c	# Not functional yet in 1.12.2
DESTROY;minecraft:cyan_glazed_terracotta;c
DESTROY;minecraft:purple_glazed_terracotta;c
DESTROY;minecraft:blue_glazed_terracotta;c
DESTROY;minecraft:brown_glazed_terracotta;c
DESTROY;minecraft:green_glazed_terracotta;c
DESTROY;minecraft:red_glazed_terracotta;c
DESTROY;minecraft:black_glazed_terracotta;c
DESTROY;minecraft:white_concrete;c			# Not functional yet in 1.12.2
DESTROY;minecraft:orange_concrete;c			# Not functional yet in 1.12.2
DESTROY;minecraft:magenta_concrete;c			# Not functional yet in 1.12.2
DESTROY;minecraft:light_blue_concrete;c			# Not functional yet in 1.12.2
DESTROY;minecraft:yellow_concrete;c			# Not functional yet in 1.12.2
DESTROY;minecraft:lime_concrete;c			# Not functional yet in 1.12.2
DESTROY;minecraft:pink_concrete;c			# Not functional yet in 1.12.2
DESTROY;minecraft:gray_concrete;c			# Not functional yet in 1.12.2
DESTROY;minecraft:light_gray_concrete;c			# Not functional yet in 1.12.2
DESTROY;minecraft:cyan_concrete;c			# Not functional yet in 1.12.2
DESTROY;minecraft:purple_concrete;c			# Not functional yet in 1.12.2
DESTROY;minecraft:blue_concrete;c			# Not functional yet in 1.12.2
DESTROY;minecraft:brown_concrete;c			# Not functional yet in 1.12.2
DESTROY;minecraft:green_concrete;c			# Not functional yet in 1.12.2
DESTROY;minecraft:red_concrete;c			# Not functional yet in 1.12.2
DESTROY;minecraft:black_concrete;c			# Not functional yet in 1.12.2
DESTROY;minecraft:white_concrete_powder;c		# Not functional yet in 1.12.2
DESTROY;minecraft:orange_concrete_powder;c		# Not functional yet in 1.12.2
DESTROY;minecraft:magenta_concrete_powder;c		# Not functional yet in 1.12.2
DESTROY;minecraft:light_blue_concrete_powder;c		# Not functional yet in 1.12.2
DESTROY;minecraft:yellow_concrete_powder;c		# Not functional yet in 1.12.2
DESTROY;minecraft:lime_concrete_powder;c		# Not functional yet in 1.12.2
DESTROY;minecraft:pink_concrete_powder;c		# Not functional yet in 1.12.2
DESTROY;minecraft:gray_concrete_powder;c		# Not functional yet in 1.12.2
DESTROY;minecraft:light_gray_concrete_powder;c		# Not functional yet in 1.12.2
DESTROY;minecraft:cyan_concrete_powder;c		# Not functional yet in 1.12.2
DESTROY;minecraft:purple_concrete_powder;c		# Not functional yet in 1.12.2
DESTROY;minecraft:blue_concrete_powder;c		# Not functional yet in 1.12.2
DESTROY;minecraft:brown_concrete_powder;c		# Not functional yet in 1.12.2
DESTROY;minecraft:green_concrete_powder;c		# Not functional yet in 1.12.2
DESTROY;minecraft:red_concrete_powder;c			# Not functional yet in 1.12.2
DESTROY;minecraft:black_concrete_powder;c		# Not functional yet in 1.12.2
DESTROY;minecraft:blue_ice;c				# Not functional yet in 1.12.2
DESTROY;minecraft:conduit;c				# Not functional yet in 1.12.2
DESTROY;minecraft:iron_door;c
DESTROY;minecraft:oak_door;c				# Not functional yet in 1.12.2
DESTROY;minecraft:spruce_door;c
DESTROY;minecraft:birch_door;c
DESTROY;minecraft:jungle_door;c
DESTROY;minecraft:acacia_door;c
DESTROY;minecraft:dark_oak_door;c
DESTROY;minecraft:repeater;c				# Not functional yet in 1.12.2
DESTROY;minecraft:comparator;c				# Not functional yet in 1.12.2
DESTROY;minecraft:structure_block;5;;deny
DESTROY;minecraft:painting;c				# Not functional yet in 1.12.2
DESTROY;minecraft:sign;c				# Not functional yet in 1.12.2
USE;minecraft:water_bucket;c				# Not functional yet in 1.12.2
USE;minecraft:lava_bucket;c				# Not functional yet in 1.12.2
USE;minecraft:pufferfish_bucket;c			# Not functional yet in 1.12.2
USE;minecraft:salmon_bucket;c				# Not functional yet in 1.12.2
USE;cod_bucket;c					# Not functional yet in 1.12.2
USE;tropical_fish_bucket;c				# Not functional yet in 1.12.2
DESTROY;minecraft:white_bed;c				# Not functional yet in 1.12.2
DESTROY;minecraft:orange_bed;c				# Not functional yet in 1.12.2
DESTROY;minecraft:magenta_bed;c				# Not functional yet in 1.12.2
DESTROY;minecraft:light_blue_bed;c			# Not functional yet in 1.12.2
DESTROY;minecraft:yellow_bed;c				# Not functional yet in 1.12.2
DESTROY;minecraft:lime_bed;c				# Not functional yet in 1.12.2
DESTROY;minecraft:pink_bed;c				# Not functional yet in 1.12.2
DESTROY;minecraft:gray_bed;c				# Not functional yet in 1.12.2
DESTROY;minecraft:light_gray_bed;c			# Not functional yet in 1.12.2
DESTROY;minecraft:cyan_bed;c				# Not functional yet in 1.12.2
DESTROY;minecraft:purple_bed;c				# Not functional yet in 1.12.2
DESTROY;minecraft:blue_bed;c				# Not functional yet in 1.12.2
DESTROY;minecraft:brown_bed;c				# Not functional yet in 1.12.2
DESTROY;minecraft:green_bed;c				# Not functional yet in 1.12.2
DESTROY;minecraft:red_bed;c				# Not functional yet in 1.12.2
DESTROY;minecraft:black_bed;c				# Not functional yet in 1.12.2
DESTROY;minecraft:item_frame;c
INTERACT;minecraft:item_frame;c			
DESTROY;minecraft:flower_pot;c
INTERACT;minecraft:flower_pot;c
DESTROY;minecraft:skeleton_skull;c			# Not functional yet in 1.12.2
DESTROY;minecraft:wither_skeleton_skull;c		# Not functional yet in 1.12.2
DESTROY;minecraft:player_head;c				# Not functional yet in 1.12.2
DESTROY;minecraft:zombie_head;c				# Not functional yet in 1.12.2
DESTROY;minecraft:creeper_head;c			# Not functional yet in 1.12.2
DESTROY;minecraft:dragon_head;5;;deny			# Not functional yet in 1.12.2
USE;minecraft:tnt_minecart;c				# Not functional yet in 1.12.2
DESTROY;minecraft:command_block_minecart;5;;deny	# Not functional yet in 1.12.2
DESTROY;minecraft:white_banner;c			# Not functional yet in 1.12.2
DESTROY;minecraft:orange_banner;c			# Not functional yet in 1.12.2
DESTROY;minecraft:magenta_banner;c			# Not functional yet in 1.12.2
DESTROY;minecraft:light_blue_banner;c			# Not functional yet in 1.12.2
DESTROY;minecraft:yellow_banner;c			# Not functional yet in 1.12.2
DESTROY;minecraft:lime_banner;c				# Not functional yet in 1.12.2
DESTROY;minecraft:pink_banner;c				# Not functional yet in 1.12.2
DESTROY;minecraft:gray_banner;c				# Not functional yet in 1.12.2
DESTROY;minecraft:light_gray_banner;c			# Not functional yet in 1.12.2
DESTROY;minecraft:cyan_banner;c				# Not functional yet in 1.12.2
DESTROY;minecraft:purple_banner;c			# Not functional yet in 1.12.2
DESTROY;minecraft:blue_banner;c				# Not functional yet in 1.12.2
DESTROY;minecraft:brown_banner;c			# Not functional yet in 1.12.2
DESTROY;minecraft:green_banner;c			# Not functional yet in 1.12.2
DESTROY;minecraft:red_banner;c				# Not functional yet in 1.12.2
DESTROY;minecraft:black_banner;c			# Not functional yet in 1.12.2
DESTROY;minecraft:end_crystal;5;;deny			# Not functional yet in 1.12.2
DESTROY;minecraft:wall_torch;c				# Not functional yet in 1.12.2
DESTROY;minecraft:redstone_wire;c			
DESTROY;minecraft:wall_sign;c
DESTROY;minecraft:redstone_wall_torch;c			# Not functional yet in 1.12.2
DESTROY;minecraft:nether_portal;c			# Not functional yet in 1.12.2
DESTROY;minecraft:attached_pumpkin_stem;c		# Not functional yet in 1.12.2
DESTROY;minecraft:attached_melon_stem;c			# Not functional yet in 1.12.2
DESTROY;minecraft:pumpkin_stem;c
DESTROY;minecraft:melon_stem;c
DESTROY;minecraft:end_portal;c				# Not functional yet in 1.12.2
DESTROY;minecraft:tripwire;c
INTERACT;minecraft:potted_oak_sapling;c			# Not functional yet in 1.12.2
INTERACT;minecraft:potted_spruce_sapling;c		# Not functional yet in 1.12.2
INTERACT;minecraft:potted_birch_sapling;c		# Not functional yet in 1.12.2
INTERACT;minecraft:potted_jungle_sapling;c		# Not functional yet in 1.12.2
INTERACT;minecraft:potted_acacia_sapling;c		# Not functional yet in 1.12.2
INTERACT;minecraft:potted_dark_oak_sapling;c		# Not functional yet in 1.12.2
INTERACT;minecraft:potted_fern;c			# Not functional yet in 1.12.2
INTERACT;minecraft:potted_dandelion;c			# Not functional yet in 1.12.2
INTERACT;minecraft:potted_poppy;c			# Not functional yet in 1.12.2
INTERACT;minecraft:potted_blue_orchid;c			# Not functional yet in 1.12.2
INTERACT;minecraft:potted_allium;c			# Not functional yet in 1.12.2
INTERACT;minecraft:potted_azure_bluet;c			# Not functional yet in 1.12.2
INTERACT;minecraft:potted_red_tulip;c			# Not functional yet in 1.12.2
INTERACT;minecraft:potted_orange_tulip;c		# Not functional yet in 1.12.2
INTERACT;minecraft:potted_white_tulip;c			# Not functional yet in 1.12.2
INTERACT;minecraft:potted_pink_tulip;c			# Not functional yet in 1.12.2
INTERACT;minecraft:potted_oxeye_daisy;c			# Not functional yet in 1.12.2
INTERACT;minecraft:potted_red_mushroom;c		# Not functional yet in 1.12.2
INTERACT;minecraft:potted_brown_mushroom;c		# Not functional yet in 1.12.2
INTERACT;minecraft:potted_dead_bush;c			# Not functional yet in 1.12.2
INTERACT;minecraft:potted_potted_cactus;c		# Not functional yet in 1.12.2
DESTROY;minecraft:skeleton_wall_skull;c			# Not functional yet in 1.12.2
DESTROY;minecraft:wither_skeleton_wall_skull;c		# Not functional yet in 1.12.2
DESTROY;minecraft:zombie_wall_head;c			# Not functional yet in 1.12.2
DESTROY;minecraft:player_wall_head;c			# Not functional yet in 1.12.2
DESTROY;minecraft:creeper_wall_head;c			# Not functional yet in 1.12.2
DESTROY;minecraft:dragon_wall_head;c			# Not functional yet in 1.12.2
DESTROY;minecraft:white_wall_banner;c			# Not functional yet in 1.12.2
DESTROY;minecraft:orange_wall_banner;c			# Not functional yet in 1.12.2
DESTROY;minecraft:magenta_wall_banner;c			# Not functional yet in 1.12.2
DESTROY;minecraft:light_blue_wall_banner;c		# Not functional yet in 1.12.2
DESTROY;minecraft:yellow_wall_banner;c			# Not functional yet in 1.12.2
DESTROY;minecraft:lime_wall_banner;c			# Not functional yet in 1.12.2
DESTROY;minecraft:pink_wall_banner;c			# Not functional yet in 1.12.2
DESTROY;minecraft:gray_wall_banner;c			# Not functional yet in 1.12.2
DESTROY;minecraft:light_gray_wall_banner;c		# Not functional yet in 1.12.2
DESTROY;minecraft:cyan_wall_banner;c			# Not functional yet in 1.12.2
DESTROY;minecraft:purple_wall_banner;c			# Not functional yet in 1.12.2
DESTROY;minecraft:blue_wall_banner;c			# Not functional yet in 1.12.2
DESTROY;minecraft:brown_wall_banner;c			# Not functional yet in 1.12.2
DESTROY;minecraft:green_wall_banner;c			# Not functional yet in 1.12.2
DESTROY;minecraft:red_wall_banner;c			# Not functional yet in 1.12.2
DESTROY;minecraft:black_wall_banner;c			# Not functional yet in 1.12.2
DESTROY;minecraft:end_gateway;5;;deny			# Not functional yet in 1.12.2
DESTROY;minecraft:frosted_ice;c				# Not functional yet in 1.12.2
DESTROY;minecraft:spawner{BlockEntityTag:{SpawnData:{id:"creeper"}}};c		# Not functional yet in 1.12.2
DESTROY;minecraft:spawner{BlockEntityTag:{SpawnData:{id:"skeleton"}}};c		# Not functional yet in 1.12.2
DESTROY;minecraft:spawner{BlockEntityTag:{SpawnData:{id:"spider"}}};c		# Not functional yet in 1.12.2
DESTROY;minecraft:spawner{BlockEntityTag:{SpawnData:{id:"giant"}}};c		# Not functional yet in 1.12.2
DESTROY;minecraft:spawner{BlockEntityTag:{SpawnData:{id:"zombie"}}};c		# Not functional yet in 1.12.2
DESTROY;minecraft:spawner{BlockEntityTag:{SpawnData:{id:"slime"}}};c		# Not functional yet in 1.12.2
DESTROY;minecraft:spawner{BlockEntityTag:{SpawnData:{id:"ghast"}}};c		# Not functional yet in 1.12.2
DESTROY;minecraft:spawner{BlockEntityTag:{SpawnData:{id:"zombie_pigman"}}};c	# Not functional yet in 1.12.2
DESTROY;minecraft:spawner{BlockEntityTag:{SpawnData:{id:"enderman"}}};c		# Not functional yet in 1.12.2
DESTROY;minecraft:spawner{BlockEntityTag:{SpawnData:{id:"cave_spider"}}};c	# Not functional yet in 1.12.2
DESTROY;minecraft:spawner{BlockEntityTag:{SpawnData:{id:"silverfish"}}};c	# Not functional yet in 1.12.2
DESTROY;minecraft:spawner{BlockEntityTag:{SpawnData:{id:"blaze"}}};c		# Not functional yet in 1.12.2
DESTROY;minecraft:spawner{BlockEntityTag:{SpawnData:{id:"magma_cube"}}};c	# Not functional yet in 1.12.2
DESTROY;minecraft:spawner{BlockEntityTag:{SpawnData:{id:"ender_dragon"}}};c	# Not functional yet in 1.12.2
DESTROY;minecraft:spawner{BlockEntityTag:{SpawnData:{id:"wither"}}};c		# Not functional yet in 1.12.2
DESTROY;minecraft:spawner{BlockEntityTag:{SpawnData:{id:"bat"}}};c		# Not functional yet in 1.12.2
DESTROY;minecraft:spawner{BlockEntityTag:{SpawnData:{id:"witch"}}};c		# Not functional yet in 1.12.2
DESTROY;minecraft:spawner{BlockEntityTag:{SpawnData:{id:"pig"}}};c		# Not functional yet in 1.12.2
DESTROY;minecraft:spawner{BlockEntityTag:{SpawnData:{id:"sheep"}}};c		# Not functional yet in 1.12.2
DESTROY;minecraft:spawner{BlockEntityTag:{SpawnData:{id:"cow"}}};c		# Not functional yet in 1.12.2
DESTROY;minecraft:spawner{BlockEntityTag:{SpawnData:{id:"chicken"}}};c		# Not functional yet in 1.12.2
DESTROY;minecraft:spawner{BlockEntityTag:{SpawnData:{id:"squid"}}};c		# Not functional yet in 1.12.2
DESTROY;minecraft:spawner{BlockEntityTag:{SpawnData:{id:"mooshroom"}}};c	# Not functional yet in 1.12.2
DESTROY;minecraft:spawner{BlockEntityTag:{SpawnData:{id:"snow_golem"}}};c	# Not functional yet in 1.12.2
DESTROY;minecraft:spawner{BlockEntityTag:{SpawnData:{id:"ocelot"}}};c		# Not functional yet in 1.12.2
DESTROY;minecraft:spawner{BlockEntityTag:{SpawnData:{id:"iron_golem"}}};c	# Not functional yet in 1.12.2
DESTROY;minecraft:spawner{BlockEntityTag:{SpawnData:{id:"horse"}}};c		# Not functional yet in 1.12.2
DESTROY;minecraft:wither_rose;c				# Not functional yet in 1.12.2
DESTROY;minecraft:smooth_stone_slab;c			# Not functional yet in 1.12.2
DESTROY;minecraft:cut_sandstone_slab;c			# Not functional yet in 1.12.2
DESTROY;minecraft:cut_red_sandstone_slab;c		# Not functional yet in 1.12.2
DESTROY;minecraft:brick_wall;c				# Not functional yet in 1.12.2
DESTROY;minecraft:prismarine_wall;c			# Not functional yet in 1.12.2
DESTROY;minecraft:red_sandstone_wall;c			# Not functional yet in 1.12.2
DESTROY;minecraft:mossy_stone_brick_wall;c		# Not functional yet in 1.12.2
DESTROY;minecraft:granite_wall;c			# Not functional yet in 1.12.2
DESTROY;minecraft:stone_brick_wall;c			# Not functional yet in 1.12.2
DESTROY;minecraft:nether_brick_wall;c			# Not functional yet in 1.12.2
DESTROY;minecraft:andesite_wall;c			# Not functional yet in 1.12.2
DESTROY;minecraft:red_nether_brick_wall;c		# Not functional yet in 1.12.2
DESTROY;minecraft:sandstone_wall;c			# Not functional yet in 1.12.2
DESTROY;minecraft:end_stone_brick_wall;c		# Not functional yet in 1.12.2
DESTROY;minecraft:diorite_wall;c			# Not functional yet in 1.12.2
DESTROY;minecraft:polished_granite_stairs;c		# Not functional yet in 1.12.2
DESTROY;minecraft:smooth_red_sandstone_stairs;c		# Not functional yet in 1.12.2
DESTROY;minecraft:mossy_stone_brick_stairs;c		# Not functional yet in 1.12.2
DESTROY;minecraft:polished_diorite_stairs;c		# Not functional yet in 1.12.2
DESTROY;minecraft:mossy_cobblestone_stairs;c		# Not functional yet in 1.12.2
DESTROY;minecraft:end_stone_brick_stairs;c		# Not functional yet in 1.12.2
DESTROY;minecraft:stone_stairs;c			# Not functional yet in 1.12.2
DESTROY;minecraft:smooth_sandstone_stairs;c		# Not functional yet in 1.12.2
DESTROY;minecraft:smooth_quartz_stairs;c		# Not functional yet in 1.12.2
DESTROY;minecraft:granite_stairs;c			# Not functional yet in 1.12.2
DESTROY;minecraft:andesite_stairs;c			# Not functional yet in 1.12.2
DESTROY;minecraft:red_nether_brick_stairs;c		# Not functional yet in 1.12.2
DESTROY;minecraft:polished_andesite_stairs;c		# Not functional yet in 1.12.2
DESTROY;minecraft:diorite_stairs;c			# Not functional yet in 1.12.2
DESTROY;minecraft:polished_granite_slab;c		# Not functional yet in 1.12.2
DESTROY;minecraft:smooth_red_sandstone_slab;c		# Not functional yet in 1.12.2
DESTROY;minecraft:mossy_stone_brick_slab;c		# Not functional yet in 1.12.2
DESTROY;minecraft:polished_diorite_slab;c		# Not functional yet in 1.12.2
DESTROY;minecraft:mossy_cobblestone_slab;c		# Not functional yet in 1.12.2
DESTROY;minecraft:end_stone_brick_slab;c		# Not functional yet in 1.12.2
DESTROY;minecraft:smooth_sandstone_slab;c		# Not functional yet in 1.12.2
DESTROY;minecraft:smooth_quartz_slab;c			# Not functional yet in 1.12.2
DESTROY;minecraft:granite_slab;c			# Not functional yet in 1.12.2
DESTROY;minecraft:andesite_slab;c			# Not functional yet in 1.12.2
DESTROY;minecraft:red_nether_brick_slab;c		# Not functional yet in 1.12.2
DESTROY;minecraft:polished_andesite_slab;c		# Not functional yet in 1.12.2
DESTROY;minecraft:diorite_slab;c			# Not functional yet in 1.12.2
DESTROY;minecraft:scaffolding;c				# Not functional yet in 1.12.2
DESTROY;minecraft:jigsaw;c				# Not functional yet in 1.12.2
DESTROY;minecraft:composter;c				# Not functional yet in 1.12.2
DESTROY;minecraft:oak_sign;c				# Not functional yet in 1.12.2
DESTROY;minecraft:spruce_sign;c				# Not functional yet in 1.12.2
DESTROY;minecraft:birch_sign;c				# Not functional yet in 1.12.2
DESTROY;minecraft:jungle_sign;c				# Not functional yet in 1.12.2
DESTROY;minecraft:acacia_sign;c				# Not functional yet in 1.12.2
DESTROY;minecraft:dark_oak_sign;c			# Not functional yet in 1.12.2
DESTROY;minecraft:loom;c				# Not functional yet in 1.12.2
DESTROY;minecraft:barrel;c				# Not functional yet in 1.12.2
DESTROY;minecraft:smoker;c				# Not functional yet in 1.12.2
DESTROY;minecraft:blast_furnace;c			# Not functional yet in 1.12.2
DESTROY;minecraft:cartography_table;c			# Not functional yet in 1.12.2
DESTROY;minecraft:fletching_table;c			# Not functional yet in 1.12.2
DESTROY;minecraft:grindstone;c				# Not functional yet in 1.12.2
DESTROY;minecraft:lectern;c				# Not functional yet in 1.12.2
DESTROY;minecraft:smithing_table;c			# Not functional yet in 1.12.2
DESTROY;minecraft:stonecutter;c				# Not functional yet in 1.12.2
DESTROY;minecraft:bell;c				# Not functional yet in 1.12.2
DESTROY;minecraft:lantern;c				# Not functional yet in 1.12.2
DESTROY;minecraft:campfire;c				# Not functional yet in 1.12.2


